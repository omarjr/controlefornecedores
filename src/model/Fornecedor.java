
package model;

import java.util.ArrayList;

public class Fornecedor {
    
    protected String nome;
    protected ArrayList<String> telefones;
    private Endereco endereco;
    private ArrayList<Produto> produtos;
    
    public Fornecedor (String nome, String telefone){
        this.nome = nome;
        telefones.add(telefone);
}

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ArrayList<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(ArrayList<Produto> produtos) {
        this.produtos = produtos;
    }

    public ArrayList<String> getTelefone() {
        return telefones;
    }

    public void setTelefone(ArrayList<String> telefone) {
        this.telefones = telefone;
    }
    
    
}
