/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controle;

import java.util.ArrayList;
import model.Fornecedor;
import model.PessoaFisica;
import model.PessoaJuridica;
import model.Produto;

/**
 *
 * @author omarfsjunior
 */
public class ControleProduto {
    private ArrayList <Produto> listaProdutos;
    
    public ControleProduto(){
        listaProdutos = new ArrayList<Produto>();
     
}
    
        public String adicionar(Produto produto){
            listaProdutos.add(produto);
            return "Produto adicionado com sucesso!";
        }
        
        public String remover(Produto produto){
            listaProdutos.add(produto);
            return "Produto removido com sucesso!";
        }
    
        public Produto pesquisar(String nome) {
            for (Produto mercadoria: listaProdutos) {
                if (mercadoria.getNome().equalsIgnoreCase(nome)) return mercadoria;
            }
            return null;
        }
}
